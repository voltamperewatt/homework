/*
Cache Simulator
Level one L1 and level two L2 cache parameters are read from file (block size, line per set and set per cache).
The 32 bit address is divided into tag bits (t), set index bits (s) and block offset bits (b)
s = log2(#sets)   b = log2(block size)  t=32-s-b
*/
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>
#include <stdlib.h>
#include <cmath>
#include <bitset>
#include <iterator>
using namespace std;

#define INST_WIDTH 32
//access state:
#define NA 0 // no action
#define RH 1 // read hit
#define RM 2 // read miss
#define WH 3 // Write hit
#define WM 4 // write miss

int z = 0;

void initialize(vector<int> vec,int n){
    for(z=0;z<n;z++){
        vec.push_back(0);
    }
}

void printvector(vector<int> vec){
    for(z=0; z<vec.size(); z++){
        cout<<vec[z]<<" ";
    }
    cout<<"\n";
}

struct config{
    int L1blocksize;
    int L1setsize;
    int L1size;
    int L2blocksize;
    int L2setsize;
    int L2size;
};

/* you can define the cache class here, or design your own data structure for L1 and L2 cache
class cache {

      }
*/


class cache{
    public:
        /*
        offset_width: determines the byte of data inside block for block size > 1
        index_width: determines the position of block in cache
        tag_width: determines length of tag, width decreases with increase in no of ways
        way_size: determines number of ways for set associative cache
        set_size: not sure what this does
        nblocks: no. of blocks in cache
        blocks_per_way: no. of blocks per way
        tag_value: stores value of tag at specific cache position
        tag_index: stores position of given tag value
        offset_position: stores block offset of given tag value
        mask: to extract offset_position from tag_index
        */
        int offset_width, index_width, tag_width, way_size, set_size, nblocks, blocks_per_way;
        int tag_value, i, tag_index, offset_position, temp;
        int mask = 0;

        vector<int> LRU;

        vector<int> nth_way;
        vector<vector<int>> ways;

        int p = 0, q = 0, r = 0, s= 0;
        cache(int block_size, int nways, int cache_size){
        // Test values of 8,1,16 = 0,
            cout<<nways<<"\n";
            // cout<<block_size<<" "<<nways<<" "<<cache_size<<"\n";
            if(nways){
                cache_size = cache_size<<10;
                // defines number of blocks in cache and number of blocks per way
                nblocks = cache_size/block_size;
                blocks_per_way = nblocks/nways;

                way_size = nways;
                // defining one block
                // initialize(block, block_size);
                vector<int> nth_way(blocks_per_way,0);
                // for(p = 0; p<block.size();p++){
                //     cout<<block[p];
                // }
                // printvector(block);
                // cout<<block.size()<<" \n";
                // defining number /of blocks per nth way, i.e one way
                vector<vector<int>> ways(nways,nth_way);
                // for(q = 0; q < nth_way.size(); q++){
                //     for(p = 0; p<nth_way[q].size();p++){
                //         cout<<nth_way[q][p];
                //     }
                // }
                cout<<nth_way.size()<<" ";
                // defining all ways
                // vector<vector<vector<int>>> ways(nways,nth_way);
                // cout<<ways.size()<<"\n";
                offset_width = log2(block_size);
                index_width = log2(cache_size/block_size) + offset_width;
                tag_width = INST_WIDTH - index_width;
                // for(int i = 0; i<offset_width; i++){
                //   mask = mask<<1;
                //   mask |= 0x01;
                //   // cout<<mask<<"\n";
                // }
            }
            else{
                nblocks = cache_size/block_size;
                blocks_per_way = nblocks/nways;
                cache_size = cache_size<<10;

                // defining one block
                vector<int> nth_way(blocks_per_way,0);
                // defining number of blocks per nth way, i.e one way
                vector<vector<int>> ways(nways,nth_way);
                // defining all ways
                // vector<vector<vector<int>>> ways(nways,nth_way);

                offset_width = 0;
                index_width = log2(cache_size/block_size) + offset_width;
                tag_width = INST_WIDTH - index_width;
            }
            cout<<"Cache formed \n";
            // cout<<"Cache formed "<<offset_width<<" "<<index_width<<" "<<tag_width<<"\n";
        }

        int readCache(bitset<32> address){
          // cout<<"Entered readCache function\n";
          // return NA;
          if(way_size == 1){
            tag_value = address.to_ulong()>>index_width;
            tag_index = address.to_ulong() % (2^index_width);
            // offset_position = address.to_ulong() & mask;
            // cout<<ways.size()<<" "<<way_size<<" "<<tag_value<<" "<<tag_index<<" "<<mask<<" "<<offset_position<<"\n";
            // if(ways[tag_index][offset_position] == tag_value){
            //   return RH;
            // }
          }
           else if(way_size > 1){
            tag_value  = address.to_ulong()>>index_width;
            tag_index = address.to_ulong() % (2^index_width);
            // offset_position = address.to_ulong() & mask;
            // cout<<"Tag Value: "<<tag_value<<" Tag Index: "<<tag_index<<" Offset Position: "<<offset_position<<"\n
            // cout<<ways.size()<<" "<<way_size<<" "<<tag_value<<" "<<tag_index<<" "<<mask<<" "<<offset_position<<"\n";
            for(i = 0; i< way_size; i++){
                // cout<<this->ways[i][tag_index].size()<<" ";
              if(ways[i][tag_index] == tag_value){
                LRU.push_back(i);
                return RH;
              }
              else{
            //     // update cache to new value
            //     // Finding not LRU by subtracting from mask.
            //     // If directly NOT then becomes negative.
            //     // ways[mask-(LRU.back() & mask)][tag_index][offset_position] = tag_value;
                return RM;
              }
            }
          }
            else{
            //   // update cache to new value
              return RM;
            }

        }
        int writeCache(bitset<32> address){
        //   cout<<"Entered writeCache function\n";
            return NA;
        //     if(temp == address.to_ulong()) return NA;
        //     int cache_index;
        //     if(way_size > 1){
        //       /*
        //       Extract offseted index. Check in each way.
        //       If in a particular way, at particular index, tag is zero, then update.
        //       Then append index way to LRU vector.
        //       Find NOT LRU.
        //       */
        //       tag_value  = address.to_ulong()>>index_width;
        //       tag_index = address.to_ulong() % (2^index_width);
        //       offset_position = address.to_ulong() & mask;
        //       for(i = 0; i< way_size; i++){
        //         if(ways[i][tag_index][offset_position] == tag_value){
        //           LRU.push_back(i);
        //           return WH;
        //         }
        //         else{
        //           // update cache to new value
        //           // Finding not LRU by subtracting from mask.
        //           // If directly NOT then becomes negative.
        //           ways[mask-(LRU.back() & mask)][tag_index][offset_position] = tag_value;
        //           return WM;
        //         }
        //       }
        //     }
        //     /* If no. of ways is one, then it is direct mapped cache.
        //     So, 15 index bits and 17 tag bits. For index, there can be 2^15 tags.
        //     To find which cache index associated with tag, find mod of
        //     */
        //     else if(way_size == 1){
        //         cache_index = address.to_ulong() & 0x7FFF;
        //         tag_value = address.to_ulong()>>index_width;
        //         tag_index = address.to_ulong() % (2^index_width);
        //         offset_position = address.to_ulong() & mask;
        //         if(ways[0][tag_index][offset_position] == tag_value){
        //           return WH;
        //         }
        //         else{
        //           // update cache to new value
        //           ways[0][tag_index][offset_position] = tag_value;
        //           return WM;
        //         }
        //
        //      }
        //     else if(way_size == 0){
        //
        //     }
        //     temp = address.to_ulong();
        }
};

// int main(int argc, char* argv[]){
int main(){
    config cacheconfig;
    ifstream cache_params;
    string dummyLine;
    cache_params.open("cacheconfig.txt");
    while(!cache_params.eof())  // read config file
    {
      cache_params>>dummyLine;
      cache_params>>cacheconfig.L1blocksize;
      cache_params>>cacheconfig.L1setsize;
      cache_params>>cacheconfig.L1size;
      cache_params>>dummyLine;
      cache_params>>cacheconfig.L2blocksize;
      cache_params>>cacheconfig.L2setsize;
      cache_params>>cacheconfig.L2size;
    }

    cache L1 = cache(cacheconfig.L1blocksize, cacheconfig.L1setsize, cacheconfig.L1size);
    cache L2 = cache(cacheconfig.L2blocksize, cacheconfig.L2setsize, cacheconfig.L2size);
    // cout << L1.getblock();
   // Implement by you:
   // initialize the hirearch cache system with those configs
   // probably you may define a Cache class for L1 and L2, or any data structure you like




  int L1AcceState = 0; // L1 access state variable, can be one of NA, RH, RM, WH, WM;
  int L2AcceState = 0; // L2 access state variable, can be one of NA, RH, RM, WH, WM;

    string argv = "trace_small.txt";
    ifstream traces;
    ofstream tracesout;
    string outname;
    outname = string(argv) + ".out";

    traces.open(argv); //open traces.txt file
    tracesout.open(outname.c_str()); //open output.out file to write outputs

    string line;
    string accesstype;  // the Read/Write access type from the memory trace;
    string xaddr;       // the address from the memory trace store in hex;
    unsigned int addr;  // the address from the memory trace store in unsigned int;
    bitset<32> accessaddr; // the address from the memory trace store in the bitset;

    if (traces.is_open()&&tracesout.is_open()){
        while (getline (traces,line)){   // read mem access file and access Cache


            istringstream iss(line);
            if (!(iss >> accesstype >> xaddr)) {break;}
            stringstream saddr(xaddr);
            saddr >> std::hex >> addr;
            accessaddr = bitset<32> (addr);
            // cout<<accessaddr.to_ulong()<<"\n";

           // access the L1 and L2 Cache according to the trace;
              if (accesstype.compare("R")==0)
              {
                // cout<<"R instruction detected\n";
                 //Implement by you:
                 // read access to the L1 Cache,
                 //  and then L2 (if required),
                 //  update the L1 and L2 access state variable;
                 L1AcceState = L1.readCache(accessaddr);
                 L2AcceState = L2.readCache(accessaddr);
                 // if(L1AcceState == RH){
                 //   L2AcceState = NA;
                 // }
                 // else if(L1AcceState = RM){
                 //   L2AcceState = L2.readCache(accessaddr);
                 // }
               }
             else
             {
                  // cout<<"W instruction detected\n";
                   //Implement by you:
                  // write access to the L1 Cache,
                  //and then L2 (if required),
                  //update the L1 and L2 access state variable;
                  L1AcceState = L1.writeCache(accessaddr);
                  L2AcceState = L2.writeCache(accessaddr);
              }
            tracesout<< L1AcceState << " " << L2AcceState << endl;  // Output hit/miss results for L1 and L2 to the output file*/;
        }
        traces.close();
        tracesout.close();
    }
    else cout<< "Unable to open trace or traceout file ";
    return 0;
}
