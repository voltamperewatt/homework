/*
Cache Simulator
Level one L1 and level two L2 cache parameters are read from file (block size, line per set and set per cache).
The 32 bit address is divided into tag bits (t), set index bits (s) and block offset bits (b)
s = log2(#sets)   b = log2(block size)  t=32-s-b
*/
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>
#include <stdlib.h>
#include <cmath>
#include <bitset>
#include <stdint.h>

using namespace std;
//access state:

#define NA 0 // no action
#define RH 1 // read hit
#define RM 2 // read miss
#define WH 3 // Write hit
#define WM 4 // write miss

#define INST_WIDTH 8

struct config{
    int L1blocksize;
    int L1setsize;
    int L1size;
    int L2blocksize;
    int L2setsize;
    int L2size;
};

int x = 0, y = 0, z = 0, w = 0;

class cache{
    public:
        int offset_width, index_width, tag_width, n_ways, set_size, nblocks, blocks_per_way;
        int tag_value, i, tag_index, offset_position, temp;
        int mask = 0;
        int way_size;
        unsigned long address = 0;
        vector<int> LRU;
        vector<vector<int>> ways;

        cache(int block_size, int nways, int cache_size){
            if(nways){
                cache_size = cache_size<<10;
                nblocks = cache_size/block_size;
                blocks_per_way = nblocks/nways;
                n_ways = nways;

                cout << nblocks << " " << blocks_per_way << " " << n_ways <<"\n";

                ways.resize(n_ways);
                for(z = 0; z < ways.size(); z++){
                    ways[z].resize(blocks_per_way);
                }
                for(x = 0; x < ways.size(); x++){
                    for(y = 0; y < ways[x].size(); y++){
                        ways[x][y]=5;
                    }
                }

                for(x = 0; x < ways.size(); x++){
                    for(y = 0; y < ways[x].size(); y++){
                        // cout<<ways[x][y]<<" ";
                    }
                    // cout<<"\n";
                }

                offset_width = log2(block_size);
                index_width = log2(cache_size/block_size) + offset_width;
                tag_width = INST_WIDTH - index_width;

                // cout<<offset_width<<" "<<index_width<<" "<<tag_width<<"\n";
            }
            cout<<"Cache formed \n";
        }

        int readCache(bitset<32> readAddr){
            cout<<"In readCache"<<endl;
            address = readAddr.to_ulong();
            if(n_ways){
                tag_value = address >> index_width;
                tag_index = address % (uint8_t)pow(2,index_width);
                    // cout<<ways.at(0).at(1)<<"\n";
                for(x = 0; x < n_ways; x++){
                    if(ways[x][tag_index] ==  tag_value){
                        return RH;
                    }
                    else{
                        ways[x][tag_index] = tag_value;
                        return RM;
                    }
                }
            }
            else return NA;
        }

        int writeCache(bitset<32> writeAddr){
            cout<<"In writeCache"<<endl;
            address = writeAddr.to_ulong();
            if(n_ways){
                tag_value = address >> index_width;
                tag_index = address % (uint8_t)pow(2,index_width);
                for(x = 0; x < n_ways; x++){
                    if(ways[x][tag_index] == tag_value){
                        return WH;
                    }
                    else{
                        return WM;
                    }
                }
            }
        }
};

int main(){



    config cacheconfig;
    ifstream cache_params;
    string dummyLine;
    cache_params.open("cacheconfig.txt");
    while(!cache_params.eof())  // read config file
    {
      cache_params>>dummyLine;
      cache_params>>cacheconfig.L1blocksize;
      cache_params>>cacheconfig.L1setsize;
      cache_params>>cacheconfig.L1size;
      cache_params>>dummyLine;
      cache_params>>cacheconfig.L2blocksize;
      cache_params>>cacheconfig.L2setsize;
      cache_params>>cacheconfig.L2size;
    }

  	cache L1 = cache(cacheconfig.L1blocksize, cacheconfig.L1setsize, cacheconfig.L1size);
    cache L2 = cache(cacheconfig.L2blocksize, cacheconfig.L2setsize, cacheconfig.L2size);

   // Implement by you:
   // initialize the hirearch cache system with those configs
   // probably you may define a Cache class for L1 and L2, or any data structure you like
    cout<<L1.readCache(0xF1)<<endl;
    cout<<L2.readCache(0xF1)<<endl;
    cout<<L1.readCache(0xF1)<<endl;
    cout<<L2.readCache(0xF1)<<endl;
    cout<<L1.writeCache(0xF1)<<endl;
    cout<<L2.writeCache(0xA1)<<endl;
    cout<<L1.writeCache(0xA1)<<endl;
    cout<<L2.writeCache(0xA1)<<endl;


  int L1AcceState =0; // L1 access state variable, can be one of NA, RH, RM, WH, WM;
  int L2AcceState =0; // L2 access state variable, can be one of NA, RH, RM, WH, WM;


    ifstream traces;
    ofstream tracesout;
    string outname;
    outname = "trace_small.out";

    traces.open("trace_small.txt");
    tracesout.open(outname.c_str());

    string line;
    string accesstype;  // the Read/Write access type from the memory trace;
    string xaddr;       // the address from the memory trace store in hex;
    unsigned int addr;  // the address from the memory trace store in unsigned int;
    bitset<32> accessaddr; // the address from the memory trace store in the bitset;

    if (traces.is_open()&&tracesout.is_open()){
        while (getline (traces,line)){   // read mem access file and access Cache

            istringstream iss(line);
            if (!(iss >> accesstype >> xaddr)) {break;}
            stringstream saddr(xaddr);
            saddr >> std::hex >> addr;
            accessaddr = bitset<32> (addr);


          // access the L1 and L2 Cache according to the trace;
            if (accesstype.compare("R")==0)
            {
                //Implement by you:
                // read access to the L1 Cache,
                //  and then L2 (if required),
                //  update the L1 and L2 access state variable;
                L1AcceState = L1.readCache(addr);
                if(L1AcceState == RH){
                    L2AcceState = NA;
                }
                else if(L1AcceState == RM){
                    L2AcceState = L2.readCache(addr);
                }
            }
            else
            {
                  //Implement by you:
                 // write access to the L1 Cache,
                 //and then L2 (if required),
                 //update the L1 and L2 access state variable;
                L1AcceState = L1.writeCache(addr);
                L2AcceState = L2.writeCache(addr);
            }
            tracesout<< L1AcceState << " " << L2AcceState << endl;  // Output hit/miss results for L1 and L2 to the output file;
        }
        traces.close();
        tracesout.close();
        cout<<"Trace generated";
    }
    else cout<< "Unable to open trace or traceout file ";







    return 0;
}
